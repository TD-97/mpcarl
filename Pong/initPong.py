################################################################################
# Filename:     initPong.py
# Author:       Thomas Davis
# Date:         April, 2020
# Description:  Main code where the game of Pong is run from, the environment
#               is initialised here, the functions which get the MPC and RL
#               actions are called from here and the rule about which action
#               to take is implemented.
################################################################################

# import the relevant modules
import sys
from os import getcwd
import gym
import numpy as np
import random
import time
import copy
import cv2
from random import randint
import datetime
from pongStates import pongStates
from timeit import default_timer as timer
# need to include the other folders in the project
sys.path.append(getcwd() + "\\LastYearCode")
sys.path.append(getcwd() + "\\Algorithm")
sys.path.append(getcwd() + "\\Main")
from mpc import mpc
from newMDPvsMPC_GR_deterministic import indexToAction
from qlearning import QLearning
# import important packages
from collections import deque

zfactor = 4
downsample_factor = np.array( [2,4] )
grayscale = 2

# main body of program
def initPong(episodeN,frameN,mode,dbname,pReward,nReward,\
  predDist,learningMode):
  # parameters for paddle dynamics found using paddleLeastAbsEst.py
  alpha = 2
  beta = 4  
  if (episodeN==0):   # if episode number not specified, use frame number
    epi_frmN=frameN
    episode=False
  else:               # else use the episode number
    epi_frmN=episodeN
    episode=True
  print("Number of frames/episodes:",epi_frmN)  
  # there are four possible modes of operation
  if(mode=="model"):  # the model mode uses only no MPC or RL, simply uses
    MPCon=False       # the data-driven model to predict where the ball is
    RLon=False        # going and selects up or down depending if it above
    modelon=True      # or below the predicted point  
  elif(mode=="MPC"):  # MPC uses only the MPC, no RL agent
    MPCon=True
    RLon=False
    modelon=True
  elif(mode=="RL"):   # RL uses only RL, no MPC
    MPCon=False
    modelon=False
    RLon=True
  else:               # MPCaRL, the full probabilistic MPRL algorithm is used
    MPCon=True
    RLon=True
    modelon=True  
  # option to display the agent playing the game
  visualMode = False  
  # some parameters associated with Pong
  agentCol = [92]
  ballCol = [236]
  paddleWidth = 4
  justLeftOfPaddle = 68
  print("MPC: ",MPCon)  
  # initialise the model predictive controller
  mpc_h = mpc(alpha,beta,25,0,justLeftOfPaddle,learningMode,\
    dbname,MPCon) 
  # initialise the pong states object
  pStates = pongStates(ballCol,agentCol)
  env = gym.make("PongDeterministic-v4") # initialise the RL environment
  observation = env.reset()
  observation, reward, done, info = env.step(0) # take the initial step 
  # some variables
  i = 0
  next_move = 0
  next_index = 0
  episode_rewards,partial, game_states, game_actions = [], [], [], []
  episode_number=1
  frmN = 1
  efn=0
  reward_sum = 0
  opponent_score = 0
  agent_score = 0 
  # initialise qlearning
  rl = QLearning(ball_x=82, ball_y=82, ai_pos_y=84, v_x=11, v_y=11, n_action=3) 
  start = timer()
  action_not_same = False

  # loop through specified number of frames / episodes
  while efn<=epi_frmN:
    # downsample the observation
    downsampled,ds_nc_u = ds(observation,visualMode)

    pStates.updateStates(downsampled,i)

    # get the MPC action if the model is on
    if (modelon):
      mpcAction,predpos = mpc_h.getMPCPred(pStates)

    # get the RL action if the RL is on
    if (RLon):
      rlAction = getQPred(pStates,rl,next_index,action_not_same,pReward,nReward)

    # if in full mode, flag whether actions the same or not
    if (mode=="MPCaRL"): 
      if (rlAction != mpcAction):
        action_not_same = True
      else:
        action_not_same = False  
    
    # if in full mode, choose the action, rule implemented in choose action
    # function
    if (mode=="MPCaRL"):
      next_move,next_index = chooseAction(pStates,rlAction,mpcAction,predpos,predDist)
    if (mode=="model" or mode=="MPC"):
      # choose MPC action with 1/100 chance of random action
      ri = randint(1,100)
      if ri > 99:
        next_move = randint(-1,1)
      else:
        next_move = mpcAction
      next_index= getIndex(next_move)

    # if in RL mode choose rl action
    if (mode=="RL"):
      next_move=rlAction
      next_index=getIndex(next_move)
      game_states.append(pStates.qs)
      game_actions.append(next_index)
    elif abs(pStates.agent-predpos)<predDist:
        game_states.append(pStates.qs)
        game_actions.append(next_index)  
    
    # send chosen action to env and get response
    observation, reward, done, info = env.step(next_move)  

    # if positive or negative reward is received, update Q-Learning
    if reward<0:
        for ind, val in enumerate(game_states[len(game_states)-4:-1]):
            rl.update(val, game_actions[len(game_states)-4+ind], game_states[len(game_states)-4+ind+1], -1, 0.7, 0.1)
        game_states=[]
        game_actions=[]
        opponent_score-=(reward)
    elif reward>0:
        for ind, val in enumerate(game_states[:-1]):
            rl.update(val, game_actions[ind], game_states[ind+1], 1, 0.7, 0.1)
        game_states =[]
        game_actions=[]
        agent_score+=reward

    # show the image to the screen if in visual mode, only for debugging
    if (visualMode):
        bre = showImg(ds_nc_u,downsampled)
        if bre:
            break
    # increment counters
    i+=1
    frmN+=1
    # if counting frames, increment efn
    if not episode:
      efn+=1 
    reward_sum += reward

    # every 500 frames, give an update on the game progress 
    if ((i % 500) == 0):
        print("Episode: ",episode_number, "Frame: ",i)
        print("Opponent: ",opponent_score,"Agent: ", agent_score)  
    
    # if reached the end of an episode or the episode is 30000 frames long
    # end the episode and start a new one
    if done or i>=30000:
        observation = env.reset() # reset env
        episode_rewards.append(reward_sum)
        end = timer()
        print ('episode:',episode_number, ' reward total was ',reward_sum,\
          "Time taken:",str(datetime.timedelta(seconds=end-start)))
        print("Number of frames: ",i)
        print("Opponent: ",opponent_score,"Agent: ", agent_score)
        print("Episode rewards:")
        print(*episode_rewards)
        if ((opponent_score<21) and (agent_score<21)):
            print("GOT STUCK, DISREGARD EPISODE ",episode_number)
            partial.append(reward_sum)
        else:
            partial.append(None)
        opponent_score=0
        agent_score=0
        start = timer()
        i=0
        reward_sum = 0
        episode_number += 1
        if episode:
          efn+=1

  # finally, return the list of rewards
  return episode_rewards

# function for showing the image to the screen
def showImg(ds_nc_u,downsampled):
    # resize the downsampled version and show it on the screen
    r_dsNoColor = cv2.resize(ds_nc_u,(downsampled.shape[1]*(\
      zfactor*downsample_factor[0]),downsampled.shape[0]*\
        (zfactor*downsample_factor[1])), interpolation=cv2.INTER_AREA)
    cv2.imshow('resized downsampled', r_dsNoColor)
    #model_results.append(r_dsNoColor)
    #cv2.imshow("ds for agent",downsampled[paddlePos[0,1]:paddlePos[0,0],
      #\paddlePos[1,1]:paddlePos[1,0]])

    if cv2.waitKey(25) & 0xFF == ord('d'):
        bre = False
        if cv2.waitKey() & 0xFF == ord('q'):
            cv2.imwrite( "./Illustrations/dataModel.jpg", r_dsNoColor )
            #out = cv2.VideoWriter('./Illustrations/mpcFull3.avi',\
            # cv2.VideoWriter_fourcc(*'DIVX'),10,(r_dsNoColor.shape[1],\
            # r_dsNoColor.shape[0]))

            #for i in range(len(model_results)):
            #    out.write(model_results[i])
            #out.release()
            bre = True
        return bre

# function to downsample the received observation
def ds(observation,visualMode):
    # remove color
    dsNoColour = copy.deepcopy(observation[:,:,grayscale])
    # downsample
    downsampled = copy.deepcopy(dsNoColour[::downsample_factor[1],::downsample_factor[0]])
    if (visualMode):
        # turn grayscale back into rgb for data visualisation purposes
        ds_nc_u = cv2.cvtColor(downsampled, cv2.COLOR_GRAY2RGB)
        return downsampled,ds_nc_u
    return downsampled, None

# function to get the RL action
def getQPred(pStates,rl,next_move,action_not_same,pReward,nReward):
    #print("Q state:",pStates.qs, "\nLast Q:", pStates.last_qs)
    #print("\nNext move: ",next_move)
    if action_not_same:
        rl.update(pStates.last_qs, next_move, pStates.qs, nReward)
    else:
        rl.update(pStates.last_qs, next_move, pStates.qs, pReward)
    return indexToAction(rl.action(pStates.qs))

# function to choose action between RL and MPC
def chooseAction(pongStates, rl, mpc,predpos,predDist):
    if(abs(predpos-pongStates.agent)<predDist):
        return rl, getIndex(rl)
    else:
        return mpc, getIndex(mpc)

# index to action function
def getIndex(action):
    if action==0:
        return 0
    elif action==3:
        return 1
    else:
        return 2
    
if __name__ == '__main__':
    initPong()
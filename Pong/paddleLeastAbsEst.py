################################################################################
# Filename:     paddleLeastAbsEst.py
# Author:       Thomas Davis
# Date:         April, 2020
# Description:  Optimisation problem, solved using MATLAB, to find the 
#               parameters associated with the paddle dynamics. 
################################################################################

import sys
from os import getcwd
# need to include the other folders in the project
sys.path.append(getcwd() + "\\LastYearCode")
sys.path.append(getcwd() + "\\Algorithm")
sys.path.append(getcwd() + "\\Main")
# import important packages
from collections import deque
import gym
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as image
import random
import time
import cv2
from newMDPvsMPC_GR_deterministic import downsample,remove_background,remove_color,indexToAction
import copy
from probabilisticTrajectory import init_PT_db,addState,create_connection,getTrajectory,getTrajectoryAll
import sqlite3
from qlearning import QLearning
from gekko import GEKKO
import numpy as np
paddle_aoi_full = np.array( [[ 70, 9],[ 72, 49 ]] ).astype(int)
grayscale = 0

def findAgent(obs,col,aoi):
    pos = np.array([ [-1, -1], [-1,-1] ])
    found=False
    for i in range(aoi[0,1],aoi[1,1]):
        for j in range(aoi[0,0],aoi[1,0]):
            if (col == obs[i,j] ):
                if ( (i>=aoi[0,1] and i<=aoi[1,1]) and (j>=aoi[0,0] and j<=aoi[1,0]) ):
                    if found == False:
                        pos[0,0]=j
                        pos[1,0]=j
                        pos[0,1]=i
                        pos[1,1]=i
                        found=True
                    if j < pos[0,0]:
                        pos[0,0] = j
                    elif i < pos[0,1]:
                        pos[0,1] = i
                    elif j> pos[1,0]:
                        pos[1,0] = j
                    elif i> pos[1,1]:
                        pos[1,1] = i
    return ((pos[0,1]+pos[1,1])/2).astype(int)

# declare random control sequence
controlSeq = [0,0,0,0,0,1,0,0,0,0,0,-1,0,0,0,0,-1,-1,0,0,0,1,1,1,1,1,0,0,0,0,\
    -1,-1,1,-1,0,-1,-1,0,0,0,0,1,1,1,1,1,-1,-1,-1,0,0,0,1,1,0,0,0,-1,-1,0,0,0,\
        0,0,0,0,-1,0,0,0,-1,-1,-1,1,1,1,0,0,0,0,0]


paddle = np.zeros([1,len(controlSeq)])
paddle[0][0]=25
paddle[0][1]=25
len(paddle)

env = gym.make("PongDeterministic-v4")
observation = env.reset()
gs_agent = 92
downsample_factor = np.array( [2,4] )
zfactor = 4

# loop through the control sequence
steps = len(controlSeq)-1
i=1
while i < steps:
    next_move = indexToAction(controlSeq[i])

    observation, reward, done, info = env.step(next_move)
    i=i+1
    # allow opencv to interperet the image
    observation_rgb = cv2.cvtColor(observation, cv2.COLOR_BGR2RGB)

    # remove color
    dsNoColour = copy.deepcopy(observation_rgb[:,:,grayscale])
    # downsample
    downsampled = copy.deepcopy(dsNoColour[::downsample_factor[1],::downsample_factor[0]])

    # record the paddle position
    paddle[0][i] = findAgent(downsampled,gs_agent,paddle_aoi_full)

    # turn grayscale back into rgb for data visualisation p
    ds_nc_u = cv2.cvtColor(downsampled, cv2.COLOR_GRAY2RGB)

    r_dsNoColor = cv2.resize(ds_nc_u,(downsampled.shape[1]*(zfactor*downsample_factor[0]),downsampled.shape[0]*(zfactor*downsample_factor[1])), interpolation=cv2.INTER_AREA)
    cv2.imshow('resized downsampled', r_dsNoColor)

    if cv2.waitKey(25) & 0xFF == ord('d'):
        if cv2.waitKey() & 0xFF == ord('q'):
            break
        else:
            continue
    time.sleep(0.1)
cv2.waitKey()
cv2.destroyAllWindows()


ua = controlSeq[1:len(controlSeq)-2]
print(ua)
ub = controlSeq[0:len(controlSeq)-3]
print(ub)
pk = paddle[0][2:len(controlSeq)-1]
p_1 = paddle[0][1]

f = np.ones([1,len(controlSeq)-1])
f[0][0] = 0
f[0][1] = 0

A = np.zeros([((len(controlSeq)-3)*2), len(controlSeq)-1 ])
b = np.zeros([(len(controlSeq)-3)*2,1])
n=0
x=0
#print(pk)
while (x<len(controlSeq)-3):
    A[n][0] = -sum(ua[0:x+1])
    A[n][1] = -sum(ub[0:x+1])
    A[n+1][0] = sum(ua[0:x+1])
    A[n+1][1] = sum(ub[0:x+1])

    A[n][x+2] = -1
    A[n+1][x+2] = -1

    b[n][0] = -pk[x] + p_1
    b[n+1][0] = -p_1 + pk[x]

    n+=2
    x+=1
print("b")
print(b)
print("A")
print(A)
print("f")
print(f)
print("Paddle")
print(paddle)
#print(pk)
#print(pk_1)

import matlab.engine
eng = matlab.engine.start_matlab()

mat_A = matlab.double(A.tolist())
mat_b = matlab.double(b.tolist())
mat_f = matlab.double(f.tolist())
mat_intcon = matlab.double([1,2])

x = eng.intlinprog(mat_f,mat_intcon,mat_A,mat_b)
print("Alpha:",x[0])
print("Beta:",x[1])
print("Error at each timestep:",x[2:])

x = np.asarray(x)

pkEst = np.zeros([1,len(pk)+1],dtype=int)
pkEst[0][0]=25
for j in range(0,len(pk)):
    pkEst[0][j+1] = pkEst[0][j] + x[0]*ua[j] + x[1]*ub[j]
np.insert(pk,[0],[25])
print("Actual paddle position:")
pkReal = paddle[0][1:(len(controlSeq)-1)]
print(pkReal)

time = np.arange(1,len(controlSeq)-1,1)
print(time)
print("Estimated paddle postion:")
print(pkEst)


plt.figure(1)
plt.step(time,pkReal)
plt.plot(time,pkReal,'C0o',alpha=0.5)
plt.step(time,pkEst[0])
plt.plot(time,pkEst[0],'C1o',alpha=0.5)
plt.legend(['Actual Paddle','','Estimated Paddle',''])
plt.ylabel("Paddle Position (pixels)")
plt.xlabel("Time steps")
plt.title("Least Absolute Parameter Estimation")

plt.show()

# The Probabilistic MPRL algorithm
The probabilistic MPRL algorithm combines a common reinforcement Learning
algorithm (q-learning) with a data-driven model predictive controller (MPC)
in order to play the atari game of Pong. It produces an agent which can learn
`to win a game of Pong with minimal training. Note that this algorithm is not 
"model free".
This project was completed in part fulfillment of the requirements of my masters
in Electronic and Computer engineering.
To run the probabilistic MPRL algorithm for the game of pong, you need to run 
the ./Main/performance_test.py file. In that file there is a range of different
parameters that can be set. 
## Overview of Code Structure
Just a quick overview of the different folders in the project, this is to give
a sense of where everything is in the code.
### Main
Main is where the program is run from, there are three files in the Main
folder. The first is the "performance test" which collects results from the
Probabilistic MPRL algorithm. Another file makes the graphs using the
results stored in the results folder. Finally, the last file creates a 
database trained on a specified number of training frames. 
### Algorithm
This folder contains the mpc.py file which handles all the interaction with the
GEKKO optimisation suite, the function of this file is the model
predictive controller.\
It also contains the ProbabilisticTrajectory.py file which provides the
functionality to interact with the database, to implement the data-driven
model of the RL environment.
### Last Year's Code
This folder contains the code obtained from work completed by Meghana Raithi
last year, it handles the Q-Learning functionality and implements the
MPCaRL algorithm in its own right.
### Pong
The Pong folder is made of the pong states file, this is a class which
keeps track of the states associated with the game of Pong, the ball position,
the paddle position, etc. The initPong.py file is the main file which implements
the algorithm. This is where the evironment is instantiated, this function
calls on other files to produce the MPC action and the RL action and then
implements the rule which chooses between which to use.\
Lastly, this folder has the paddleLeastAbsEst.py, this uses MATLAB to
solve what parameter are optimum for use in the equation that dictates
the paddle dynamics.
### Results
This is just a folder of text files filled with the results collected using
the ./Main/performance_test.py program.
## Sidenote
I used the a windows version of the atari-py module found at this location 
-> https://github.com/Kojoley/atari-py

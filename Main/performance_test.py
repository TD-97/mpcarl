################################################################################
# Filename:     performance_test.py
# Author:       Thomas Davis
# Date:         April, 2020
# Description:  File to run the tests on the probabilistic MPRL algorithm.
#               done so we can vary one parameter easily
################################################################################

# import modules
import numpy
import matplotlib.pyplot as plt
import sys
from os import getcwd
# need to include the other folders in the project
sys.path.append(getcwd() + "\\Pong")
sys.path.append(getcwd()+"\\Results")
from initPong import initPong

# function which plays a set number of episodes, one of pReward, nReward or
# h can be lists, if one is, we will simulate each value on that list
def play_pong(episodeN,pReward,nReward,h,lmode,dbname):
    numberoflist=0
    listvar=0
    # check if one of the variables are lists
    if isinstance(pReward,list):
        numberoflist+=1
        listvar=1
    if isinstance(nReward,list):
        numberoflist+=1
        listvar=2
    if isinstance(h,list):
        numberoflist+=1
        listvar=3
    if(numberoflist>1):
        raise ValueError("Can only give one list to this function!")
    
    # convert the other two variables to lists of the same value
    if (listvar==1):
        listlen=len(pReward)
        pRewardl = pReward
        nRewardl = [nReward]*listlen
        hl = [h]*listlen
    elif (listvar==2):
        listlen=len(nReward)
        pRewardl = [pReward]*listlen
        nRewardl = nReward
        hl = [h]*listlen
    elif(listvar==3):
        listlen=len(h)
        pRewardl = [pReward]*listlen
        nRewardl = [nReward]*listlen
        hl = h
    # if no lists, the length will just be one
    else:
        listlen=1
        pRewardl = [pReward]*listlen
        nRewardl = [nReward]*listlen
        hl = [h]*listlen

    reward = []     # create reward list
    frameN=0        # this isn't important as long as episodeN is not zero
    mode="MPCaRL"   # set the mode to the full MPC and RL
    # loop through the list
    for i in range(listlen):
        # simulate the parameters and append the reward received to the list
        reward.append(initPong(episodeN,frameN,mode,dbname,\
          pRewardl[i],nRewardl[i],hl[i],lmode))

    return reward

episodeN = 50                   # set the number of episodes

#dbname = "pong_d4_50k.db"      # set the database
#dbname = "pong_d4_100k.db"
#dbname = "pong_d4_200k.db"
#dbname = "pong_d4_400k.db"
#filename = "MPCaRL_50k.txt"    # set the filename
#filename = "MPCaRL_100k.txt"
#filename = "MPCaRL_200k.txt"
#filename = "MPCaRL_400k.txt"


dbname = "third_attempt_PT.db"
lmode=False                     # set whether the database should be updated

h = 5
pReward=0
nReward=0
filename="MPCaRL_nominal.txt"

#h = [2,3,4,5,6] 
#pReward = 0
#nReward = 0
#filename="MPCaRL_h_real.txt"

#h = 3
#pReward = [0,0.2,0.4,0.6,0.8]
#nReward = 0
#filename="MPCaRL_rp.txt"

#h = 3
#pReward = 0
#nReward = [0,-0.2,-0.4,-0.6,-0.8]
#filename="MPCaRL_rn.txt"

rewards = play_pong(episodeN, pReward, nReward, h,lmode,dbname)

# create graphic of results
nLines = len(rewards)

# plot the results and save to a text file
with open(getcwd()+"\\Results\\"+filename, 'w') as f:
  for i in range(nLines):
    plt.plot(rewards[i],marker='o',markersize=3,linewidth=1)
    f.write("%s\n" % rewards[i])

plt.legend()
plt.ylabel('reward')
plt.xlabel('episode')
plt.show()





    






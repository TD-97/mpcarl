################################################################################
# Filename:     createDBofSizeN.py
# Author:       Thomas Davis
# Date:         April, 2020
# Description:  Simple program to create the data-driven database for the game
#               of pong, only using a set number of training frames to do so.
################################################################################

# import the necessary modules
import sys
from os import getcwd
sys.path.append(getcwd() + "\\Pong")
from initPong import initPong

episodeN=0                  # leave this null, want to go by frames
frameNumber=50000           # set number of frames to simulate
mode = "model"              # use the model mode, no controller or rl
dbname = "pong_d4_50k.db"   # set name of database
#dbname = "pong_d4_100k.db"
#dbname = "pong_d4_200k.db"
#dbname = "pong_d4_400k.db"
pReward = 0                 # leave required params null as they make no
nReward = 0                 # difference in model mode
h = 3
lmode = True                # make sure we use learning mode so 


rewards = initPong(episodeN, frameNumber, mode, \
  dbname, pReward, nReward, h,lmode)

# if episode number is null, use frame number
# need to be able to specify db
# create different modes

# model   - Only train the model, no RL or MPC.
# MPC     - the mpc playing on its own with model, no RL.
# MPCaRL  - MPC, model, RL.
# RL      - Just the RL playing on it's own.

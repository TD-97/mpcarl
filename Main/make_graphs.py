################################################################################
# Filename:     make_graphs.py
# Author:       Thomas Davis
# Date:         April, 2020
# Description:  File to make graphs using the results in the Results folder
################################################################################

# import modules
import matplotlib.pyplot as plt
from os import getcwd

# perturbing distance
#filename = "MPCRL_h_real.txt"

# perturbing rp
#filename = "MPCRL_rp.txt"

# perturbing rn
#filename = "MPCRL_rn.txt"

# perturbing db
#filename = "MPCaRL_db.txt"

# RL on own
#filename = "MPCaRL_nominal.txt"

# read the results into a list
rewards = []
with open(getcwd()+"\\Results\\"+filename, 'r') as f:
    lines = f.readlines()
    for line in lines:
        rewards.append([float(s) for s in line.strip("\n").strip("]").strip("[").split(',')])

# plot the rewards
for i in range(len(rewards)):
    plt.plot(rewards[i][0:100],marker='o',markersize=3,linewidth=1)

# print(len(rewards)) # debug statement

# create the legends for the graphs
plt.rc('text', usetex=True)
# perturbing distance
#plt.plot(rewards[0], marker='o', markerfacecolor='green', markersize=3, color='green', linewidth=1,label="$H_y=2$")
#plt.plot(rewards[1], marker='o', markerfacecolor='red', markersize=3, color='red', linewidth=1,label="$H_y=3$")
#plt.plot(rewards[2], marker='o', markerfacecolor='orange', markersize=3, color='orange', linewidth=1,label="$H_y=4$")
#plt.plot(rewards[3], marker='o', markerfacecolor='pink', markersize=3, color='pink', linewidth=1,label="$H_y=5$")
#plt.plot(rewards[4], marker='o', markerfacecolor='blue', markersize=3, color="blue", linewidth=1,label="$H_y=6$")

# perturbing rp
#plt.plot(rewards[0], marker='o', markerfacecolor='green', markersize=3, color='green', linewidth=1,label=r"$\bar{r}=0$")
#plt.plot(rewards[1], marker='o', markerfacecolor='red', markersize=3, color='red', linewidth=1,label=r"$\bar{r}=0.2$")
#plt.plot(rewards[2], marker='o', markerfacecolor='orange', markersize=3, color='orange', linewidth=1,label=r"$\bar{r}=0.4$")
#plt.plot(rewards[3], marker='o', markerfacecolor='pink', markersize=3, color='pink', linewidth=1,label=r"$\bar{r}=0.6$")
#plt.plot(rewards[4], marker='o', markerfacecolor='blue', markersize=3, color="blue", linewidth=1,label=r"$\bar{r}=0.8$")

# perturbing rn
#plt.plot(rewards[0], marker='o', markerfacecolor='green', markersize=3, color='green', linewidth=1,label=r"$\underline{r}=0$")
#plt.plot(rewards[1], marker='o', markerfacecolor='red', markersize=3, color='red', linewidth=1,label=r"$\underline{r}=0.2$")
#plt.plot(rewards[2], marker='o', markerfacecolor='orange', markersize=3, color='orange', linewidth=1,label=r"$\underline{r}=0.4$")
#plt.plot(rewards[3], marker='o', markerfacecolor='pink', markersize=3, color='pink', linewidth=1,label=r"$\underline{r}=0.6$")
#plt.plot(rewards[4], marker='o', markerfacecolor='blue', markersize=3, color="blue", linewidth=1,label=r"$\underline{r}=0.8$")

# perturbing db
#plt.plot(rewards[0], marker='o', markerfacecolor='green', markersize=3, color='green', linewidth=1,label=r"50k samples")
#plt.plot(rewards[1], marker='o', markerfacecolor='red', markersize=3, color='red', linewidth=1,label=r"100k samples")
#plt.plot(rewards[2], marker='o', markerfacecolor='orange', markersize=3, color='orange', linewidth=1,label=r"200k samples")
#plt.plot(rewards[3], marker='o', markerfacecolor='pink', markersize=3, color='pink', linewidth=1,label=r"400k samples")

# show the plot
plt.legend(loc="lower right",fontsize=15)
plt.xlabel("Episode",fontsize=15)
plt.ylabel("Reward",fontsize=15)
plt.rc("xtick", labelsize=15)
plt.rc("ytick", labelsize=15)
plt.show()